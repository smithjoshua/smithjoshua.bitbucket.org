# Physics

If you've never messed with a physics engine before, fear not!
SpriteKit comes with a powerful physics engine built in.  It is simple
enough for beginners to grasp and powerful enough to handle whatever
you throw at it.  It integrates with the scene graph and any node can
participate in the simulation to give the illusion of weight,
friction, and more.

We're going to play with this physics engine bit by bit, allowing you
to explore and learn.  By the end of this chapter, you'll have
developed the basic vocabulary of the physics engine in SpriteKit and
started to develop your next hit physics game.

You will learn about the different body types,  how to manipulate
gravity (if only we could do _that_ in real life). We'll also cover
how to apply forces to bodies and how to create dynamic and static
bodies.  We'll also cover what the heck those last two things even
mean.   Don't worry,  no one has ever been killed writing a physics
game.


## (It's the end of the) World As We Know It

With all apologies to R.E.M. we now talk about the world.  Not the
real world, rather the Physics World that will run our simulation.
Physics in games is done via some kind of physics simulation.  This
simulation isn’t a very good approximation of the real world but it’s
good enough to _look_ like it’s good enough. If we look at a game like
Angry Birds, which is the game that launched 10,000 clones and uses
physics as a critical game mechanic.  The birds are launched using a
slingshot and fly in the parabolic arc we understand innately.  The
objects on the screen collide with one another, and bounce and fall
and do all the things that real blocks do when we hit them.

All of this is done via the simulation.  Try an experiment with me:
take this book and stand it up on the table in front of you.  Find a
pen and throw it at the book.  You will see that the pen hits the book
and (hopefully) knocks the book down.  Both objects fall toward the
center of the earth at 9.8 m/s^2 and both stop falling when they come
to rest on the floor or table.  You could perform this experiment 1000
times and the pen will never pass through book leaving the book
unchanged.  This is because we live in the real world.

In the game world, though, things are a little bit different.  The pen
might appear to pass through the book if it’s going fast enough.  This
is because the physics simulation in our game is actually performing a
time-sliced set of calculations.  The position of the pen at time t =
0 is calculated, as is the position of the pen at time t + delta-t and
so on.  If the pen is going fast enough, the pen’s position will be
past the book on the next calculation, and so the pen will appear to
pass through the book.

This discrete time-slice thing is the most important thing to remember
about physics engines.

The Second Most Important Thing is that floating point numbers round
badly.  Rounding errors are a major problem for physics engines.  You
must consider the effects of rounding on your mechanic when you are
building your game.  These rounding effects are one of the reasons you
can't use a game physics engine to do orbital mechanics.

The simulation we're talking about here is represented as the
`physicsWorld` parameter on the `SKScene`.  It's there on every scene,
but if there are no physics bodies there isn't any simulating being
done.

## Our First Bodies

We're going to start out in a playground.  You can download the
complete playground used here from _URL-TBD_ or you can type in the
code.  We're using swift so there isn't a huge amount of code and
we're going to be focusing on the new concepts for nodes and scenes
that come with the physics engine.

If you're typing this into a new playground, first remove all the code
from the playground file before we begin.  The first three lines of
code are going to be imports.

TODO: this needs to be an iOS, which we get View -> Utilities -> Show
File Inspector.

```c
import UIKit
import SpriteKit
import XCPlayground
```

UIKit we import because it's the base of a lot of stuff we'll need in
swift.  Then we import SpriteKit to get access to the SpriteKit
framework.  Lastly, we import XCPlayground so that we can see what's
going on in our views as we go.  You'll need to enable the Assistant
Editor to see the real time display, which you'll find in the menu View
-> Assistant Editor -> Show Assistant Editor or the command key
Option-Cmd-Return.

First, let us create an `SKView`.  This view will hold the scene and
all the nodes.

```c
let sideLength = 500
let rect = CGRect(x: 0, y: 0, width: sideLength, height: sideLength)
let mySkview = SKView(frame: rect)
```

This won't show anything except an empty view.  We'll need to build
and add a `SKScene` to the view and present it.

```c
let mySkscene = SKScene(size: rect.size)
mySkview.presentScene(mySkscene)

XCPShowView("sceneview", mySkview)
```

That last line executes the `XCPShowView` function that displays the
contents of the view in the assistant editor. This function is only
used in playgrounds.  We have to use this instead of just looking at
the view because the playground time line only calls the `drawRect`
method of the view once.  With SpriteKit, we need to see the contents
of the view change over time, which is what the XCPShowView function
does for us.

Now let's add a box to the scene.  This box is going to also have a
physics body.

```c
let box = SKSpriteNode(color: SKColor.redColor(), size: CGSizeMake(40,
40))
box.position = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))
box.physicsBody = SKPhysicsBody(rectangleOfSize: box.frame.size)
mySkscene.addChild(box)
```

The physics body we have added is a rectangular physics body.  The
body is the same size and shape as the box node.  The body, unlike the
node's color, is not visible on the screen.  The body is there only to
interact with the physics world.

There are three basic kinds of physics bodies.  The first, is a body
with volume.  These bodies can be shapes or the alpha-mask of a
texture.

![bodies with volume](./images/bodies_with_volume.png)

The second is an edge chain.  Edge chains are special in
that they do not have volume.  The last is an edge loop.  And Edge
loop is like an edge chain, except the loop is closed and a chain can
be open.

![edge bodies](./images/edge_bodies.png)

Why does the box fall?  The physics world starts out with gravity
already setup.   All `SKNodes` have a physics world associated with
them.  Since we'd like to watch that box fall, let's go ahead and
change gravity.

```c
mySkscene.physicsWorld.gravity = CGVectorMake(0, -0.5)
```

The gravity property is a vector that describes the acceleration (in
pixels per second squared) in the x and y directions.    By default,
gravity is configured to the a vector with `dx` 0 and `dy` -9.8 which
is an approximation of the earths gravity.

This is a simulation, though, and we are free to change the values for 
gravity however we want.  In this case we set it to a very small value
(-0.5) so the box falls very slowly.  Remember, the box isn't really
"falling" at all.

Even though we've changed gravity we have another problem: the box
falls off the screen.   To fix this, we'll use on of the other physics
body types.  The scene itself is also an `SKNode` so even though it
has the physics world, it can also have a physics body itself.  We'd
like to have a wall that blocks the box from falling off the screen
and into infinity.

To build that wall, we'll add an edge loop with a rect.

```c
mySkscene.physicsBody = SKPhysicsBody(edgeLoopFromRect:
mySkview.frame)
```

The rect we'll use is the rect of the enclosing view.  That way we get
a wall and the box will not fall off the screen
anymore. Congratulations!

Now let's talk about all the properties of physics bodies that we can
use to construct games using physics as a mechanic.  In the last few
chapters, you've moved nodes around on the screen.  We can still do
that, but we can also move the physics body associated with a node.
This movement we do via forces.

We'll look at the most dramatic of the forces first: angular impulse.
An angular impulse is an instantaneous force applied so that the physics
body rotates around its moment of inertia.  If I lost there with the
physics lecture,  don't worry, when you see it in action it'll be very
clear.  

```c
box.physicsBody?.applyAngularImpulse(20)
```

The box now spins like mad and bounces around the walls.  Eventually,
the box comes to rest, though.  Now we know from Newton that an in
motion tends to stay in motion so why does the box stop?  One of the
properties of physics bodies is `angularDamping`.  This parameter is a
float between 0.0 and 1.0 (though it defaults to 0.1) that simulates a
friction-like force that slows the rotation of a physics body.

We can apply linear impulses to the bodies, too.  We do that with the
`applyImpulse` method. This method takes a vector, like gravity, and
applies an impulse along that vector.

```c
box.physicsBody?.applyImpulse(CGVectorMake(10,10))
```

This impulse hurls the box around and sends it bouncing.  A physics
body also has a `linearDamping` property that works to slow a body
even it does not interact with other bodies.  A body also has an oddly
named, but important, property called restitution.  The restitution of
an object is its bounciness.  This setting is a floating point number
between 0 and 1.0 where 1 is almost perfectly elastic (like a super-ball), and 0 is almost
perfectly in-elastic (like a bag of lead pellets).

Forces are different than impulses because they must be applied over
time. An impulse is instantaneous, like a collision. Forces are more
like a push.

Now you have seen enough of the basics of working with physics bodies
to move on to writing an actual game using physics as a game mechanic.
The game is a puzzle game where you, via swiping, bounce the hero
through the maze to get to the goal. Let's go!

## Our Princess has a problem.

Princess Pecan has a pet platypus named Platy.  Normally Platy is a
well behaved pet.  Today, however, Platy is being a bit of a pest.
Platy has pilfered the keys to the Princess' library and left several
puzzles for the Princess to solve.  We, the player of this particular
game, are the princess and must solve these puzzles if we hope to get
back into our beloved library.  These puzzles take the form of simple
mazes within which the laws of physics are the only tools we may use
to get navigate the maze and get to a piece of the the key.

![wall diagram](./images/wall_sketch.png)

We're going to lay this out using the Level Designer.  You've seen
this in earlier chapters, but we're going to use some features for the
first time with this game. You can also download the source code for this
chapter at _URLTBD_ or follow along with us here.

After we open Xcode and choose File -> New Project, we're going to
choose the "Game" template under iOS.  

![New Game Project](./images/physics_new_project.png)

We're then going to name the project and set it up using `SpriteKit`
as the game technology, `Swift` as the language, and iPhone for the
device.

![Game Project Options](./images/physics_new_project_options.png)

We then should update the `scaleMode` of the scene to be `.AspectFit`
instead of `.AspectFill` chaning this:

```c
/* Set the scale mode to scale to fit the window */
scene.scaleMode = .AspectFill
```

to this:

```c
/* Set the scale mode to scale to fit the window */
scene.scaleMode = .AspectFit
```

in the `GameViewController.swift` file. After we do that, we can
continue with our game.  We're going to lay out one of the walls. Drag
a color sprite onto the scene. and make it look like one of the walls
in the sketch (it doesn't mater which one).  You'll see the option

![Body Default Setting](./images/menu_physics_definition.png)

is currently set to `None`.  We'll change that to have a physics body
of type `Bounding Rectangle`.  This does the same thing we did earlier
in code and ads a physics body to the node that is the same size and
shape as the nodes frame.  We're also going to UNcheck the options
`Dynamic`, `Allows Rotation`, and `Affected by Gravity`.  These
settings will make the wall not fall, will prevent it from rotating,
and make it immune to gravity.  Wait a minute,  isn't 'not falling'
and 'immune to gravity' the same thing?

There are physics bodies which are dynamic. This kind of dynamic means
that the body has volume and mass. It also means that it  can be
affected by forces and collisions.  A static body, which is a body
that has the `Dynamic` setting UNchecked, is a body that has mass and
volume but is not itself affected by forces and collisions.

Now that you have your wall in place, let's throw in the Princess. Add
another Color sprite to your scene, and set it's physics body to
`Bounding Rectangle` but leave the other options set with their
defaults.

You may have noticed the `Simulate` button in the lower left of the
Level Designer.

![Simulate Button](./images/physics_scene_simulate.png)

If you tap that button,  you'll see an animated simulation of your
scene as the level designer understand it.  You'll also see the
princess fall off the screen.  That is because there is no edge wall,
and even if their was, the Level Designer doesn't know it's there.
This is a failure of the Level Designer and it makes that `Simulate`
button not as useful as it could be.

Lets go ahead and add the name property to the princess sprite and set
it to be the string "princess".  Then we can remove all the code the
template adds for us in the scene, after which your scene should look
something like this:

```c
import SpriteKit

class GameScene: SKScene {
override func didMoveToView(view: SKView) {
}

    override func touchesBegan(touches: Set<NSObject>, withEvent
    event: UIEvent) {
	}
	}
```
add the bounding walls to the scene's physics body.  You can also
change gravity here, if you'd like.  After adding the walls you scene
should look like this:

```c
class GameScene: SKScene {
override func didMoveToView(view: SKView) {
self.physicsBody = SKPhysicsBody(edgeLoopFromRect: view.frame)
}

    override func touchesBegan(touches: Set<NSObject>, withEvent
	event: UIEvent) {
	}
	}
	```

Now we need to add the method that's going to be called by our gesture
recognizer we're going to use to launch the princess.  We can add this
function to the scene and call it from the `UIPanGesture` we'll put on
the view.

```c
public func useVelocity(velocity: CGVector) {
if let princess = self.childNodeWithName("princess") {
if let body = princess.physicsBody {
body.applyImpulse(velocity)
}
}
}
```

We'll call that method from a pan action like this one:

```c
@IBAction func didPan(sender: UIPanGestureRecognizer) {
let sstate = sender.state
switch sstate {
case .Ended:
let skView = self.view as! SKView
if let scene = skView.scene as? GameScene {
let vel = sender.velocityInView(skView)
let vec = CGVectorMake(vel.x, vel.y)
scene.useVelocity(vec)
}
break
default:
break
}
}
```

Now, run the application and see that we can use the pan gesture to
launch the princess around.  After this you can add the other two walls and set them up the same
way you did the first wall.

There you have it,  you've created the prototype for your first
physics game.  That wasn't so hard now was it?  We've gone over the
basic body types,  how to change gravity, and how to apply forces to
bodies in the system.  We've also shown how to create static and
dynamic bodies.




# More Physics

<!-- Here is some swift code to turn integers into binary strings.   -->

<!-- ```c -->
<!-- func getBitString(n: Int, acstr:String) -> String { -->
<!-- var result: String; -->
<!-- if (n <= 0) { -->
<!-- result = acstr -->
<!-- } else { -->
<!-- let nval = 1 -->
<!-- if ((n & nval) == 1) { -->
<!--   result = getBitString(n >> 1, -->
<!--   acstr.stringByAppendingString("1")) -->
<!-- } else { -->
<!-- result = getBitString(n >> 1, -->
<!-- acstr.stringByAppendingString("0")) -->
<!-- } -->
<!-- } -->
<!-- return result -->
<!-- } -->

<!-- func getBitString(n: Int, resultLength: Int) -> String { -->
<!-- let bitString = getBitString(n, "") -->
<!--     let paddedResult = bitString.stringByPaddingToLength(resultLength, -->
<!-- 	withString: "0", startingAtIndex: 0) -->
<!-- 	let reversed = reverse(paddedResult) -->
<!-- 	return String(reversed) -->
<!-- } -->
<!-- ``` -->

Once there was a wise Masked Owl named Oliver.  He was only BitWise, though,
and he was Princess Pecan's math tutor.  One day he posed the
following problem to the princess:  Let's say we have an array of
10,000 integers.  What is the best way to tell if a given integer is
already in that array?

The Princes, being quite strong in her Algorithms laughed at the
easy question.  "A `for` loop and check each one. " she beamed.

```c
var foundDupe = false
let isThere = 523
let numbers = 1...10000
for number in numbers {
if (number == isThere) {
foundDupe = true
}
}
```

"That is a correct answer." Oliver said, and the princess caught the odd
specificity of the statement.  "But is it the only answer?" She
scowled for she had seen Oliver play these kinds of Socratic Owl games
before.

We do not care that the value of the number is in the array,
only that its _bits_ are present.  Because we know this, and we know
that bit-wise checks are very fast for a computer to perform we can
use the exclusive or on each value of the array and get the answer
faster.

![bit table](./images/bit_table.jpg)

"I thought ORs were never exclusive?" The princess asked,  knowing
that words often had many meanings.  And Oliver began to scratch his
lesson into the dirt...

There are four bitwise operations we have available to us: and, or,
xor, and not. Though not is a unary operator and unlike the other
three.  If we start with the simplest case, a one bit number and
another one bit number we see that:

```c
0 AND 0 = 0
1 AND 0 = 0
1 AND 1 = 1
```

We use the the `&` symbol to represent AND when talking about more
complicated situations which gives us something like this

```c
0 & 0 = 0
1 & 0 = 0
1 & 1 = 1
```

Longer bit sequences follow the same rules. For example, given the
numbers 2 and 6 we can `&` them together to get 2. In binary this
would look like this:

```c
 00010
&00110
------
 00010
```

If we take 5 `&` 7 we get 5 because:

```c
 00101
&00111
------
 00101
```

At this Point, the princess was losing patience and interrupted, "Then &
is the exclusive OR?"  "We'll get to that" Oliver encouraged.  "First
we talk about AND and OR".  The princess was not convinced, but she
folded her arms and was silent for now.  "On to Or.  Bitwise OR takes
the first bit OR the second bit. With one bit, that looks like this: 

```c
0 OR 0 = 0
1 OR 0 = 1
1 OR 1 = 1
```

OR also has a special symbol, and we use the `|` to denote OR.  

```c
0 | 0 = 0
1 | 0 = 1
1 | 1 = 1
```

If we expand to longer sequences as we did before,  we see a similar
pattern emerge. If we OR 2 with 6 we 6. In binary we
get.

```c
 00010
|00110
------
 00110
```

If we take 5 `|` 7 we get 7 because:

```c
 00101
|00111
------
 00111
```

Knowing this, what would you call an operator which would make the
following true:

```c
0 ?? 0 = 0
1 ?? 0 = 1
1 ?? 1 = 0
```

The princess thought about that for a moment and smiled shouting
"EXCLUSIVE OR!". Oliver smiled (though how you can tell an Owl is
smiling is a real trick).  "Correct" he said,  and we use the word
`XOR` to describe this and the operator is `^`.

```c
0 ^ 0 = 0
1 ^ 0 = 1
1 ^ 1 = 0
```

Expanding this to the sequences we've used before, we again see a
similar pattern emerge. If we XOR 2 with 6 we 4. In binary we get.

```c
 00010
^00110
------
 00100
```

If we take 5 `^` 7 we get 2 because:

```c
 00101
^00111
------
 00010
```

"But what about NOT?  We forgot about NOT!" The princess exclaimed,
quite pleased with herself.  "Ah, " Oliver said,  "Not is unlike the
others because NOT is unary, which means it only acts on one number."
The operator we use for not is the `~`.  This operation flips the bits
in the number, and it only operates on a single number.

```c
~0 = 1
~1 = 0
```

and so, if it take the `~` of 5 we get 26

```c
~00101
------
 11010
```

and if we take the `~` of 0 we get 31

```c
~00000
------
 11111
 ```

"OK", the princess beamed, I think I get it now.  How about this?

```c
let isThere = 523
let foundDupe = (1...1000).map({v in isThere ^ v}).reduce(false, combine: {k,v in k || v == 0})
```

## Collisions

When a ball hits the floor, it bounces off.  That bounce is a
caused by a collision.  This is also true in our physics simulation.
Unlike the real world, we can choose to make some objects collide and
some objects not.  We set this up using the `collisionBitMask`
property of the node's physics body.

The `collisionBitMask` is a bitmask that determines which categories
the node will collide with.  By default, all bits are set meaning the
body will collide with any other body.  This bitmask is `&`ed together
with the category of the other body and if the result is not 0 they
will collide. 

## Contact

Sometimes we don't want two bodies to collide but we still want to
know that they made contact.  In our game we have the Accelerator.
This accelerator will add speed to the Princess, but the she doesn't
collide with the accelerator patch (if she did, she would bounce off).
Lucky for us, contact between two bodies is handled differently than a
collision.

Like collision, we have a bitmask on each physics body that allows the
physics world to determine if they should make contact or not.  The
bitmask we use for this is the `categoryBitMask`.  This bitmask tells
the physics world what categories of object can make contact.  Objects
can make contact even when they do not collide (and vice verse).

When two physics bodies make contact with one another,  the callback
`didBeginContact` is called on the `SKPhysicsContactDelegate`.  This
method takes an object of type `SKPhysicsContact` as it's only
argument.  An `SKPhysicsContact` object has variables that describe
the two bodies that are in contact, their contact point, a vector
describing the normal of their contact, and the impulse that would be
created if they had collided.

Two bodies make contact if their contact bitmasks (found in the
`contactTestBitMask` property) 

## action in only one direction

We've got a nice little acceleration pad here.  There's just one
problem,  we only want it to accelerate the princess in one
direction.  If she comes into an acceleration pad from the "wrong"
direction, we'd like her to pass right through it like a
one-way-force-field.

We can do that with the `SKPhysicsContact` object.  This object
includes a member variable that describes the normal vector of the
contact point.  If your eyes glazed over a little bit on that last
sentence, fear not.  This is just a way of describing the direction
of contact.  This information will help us apply the force if it is in the
right direction or ignore it if it's not.  
