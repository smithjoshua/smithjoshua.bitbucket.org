//
//  GameScene.swift
//  PoolTableSimulation
//
//  Created by Josh Smith on 4/24/15.
//  Copyright (c) 2015 Josh Smith. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {

    func createBallAtPoint(point : CGPoint) -> SKShapeNode {
        let randSize = CGFloat(arc4random_uniform(100)+30)
        let originRect = CGRectMake(point.x, point.y, randSize, randSize)
        let ball = SKShapeNode (ellipseInRect: originRect)
        ball.fillColor = SKColor.blueColor()

        let bodyCenterPoint = CGPointMake(CGRectGetMidX(originRect), CGRectGetMidY(originRect))
        ball.physicsBody = SKPhysicsBody(circleOfRadius: randSize/2, center: bodyCenterPoint)
        return ball
    }

    override func didMoveToView(view: SKView) {
        let screenInset = CGRectInset(self.frame, 30, 30)
        let table = SKShapeNode(ellipseInRect: screenInset)
        let circlePath = UIBezierPath(ovalInRect: screenInset)
        table.physicsBody = SKPhysicsBody(edgeLoopFromPath: circlePath.CGPath)
        self.addChild(table)
        self.physicsWorld.gravity = CGVectorMake(0.0, -9.8)
        if let centerPoint = self.view?.center {
            let ball = createBallAtPoint(centerPoint)
            self.addChild(ball)
        }
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {

        for touch in (touches as! Set<UITouch>) {
            let tloc = touch.locationInNode(self)
            let ball = createBallAtPoint(tloc)
            self.addChild(ball)
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
